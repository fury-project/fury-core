﻿using Ardalis.Specification;
using System.Threading.Tasks;

namespace Fury.Core.Interface
{
    public interface IRepository<T> : IRepositoryBase<T>, IReadRepository<T> where T : class, IAggregateRoot
    {
    }
}
