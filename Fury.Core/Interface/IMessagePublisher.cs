﻿using Fury.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fury.Core.Interface
{
    public interface IMessagePublisher<T> where T : BaseIntegrationEvent
    {
        Task Publish(T messageEvent);
    }
}
