﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fury.Core.Models
{
    public abstract class BaseEntity<TId>
    {
        public TId Id { get; protected set; }
        public BaseEntity(TId id)
        {
            this.Id = id;
        }

        private List<INotification> _domainEvents = new List<INotification>();
        public IReadOnlyCollection<INotification> DomainEvents => _domainEvents?.AsReadOnly();

        public void AddDomainEvent(INotification events)
        {
            this._domainEvents.Add(events);
        }

        public void RemoveDomainEvent(INotification eventItem)
        {
            this._domainEvents?.Remove(eventItem);
        }

        public void ClearDomainEvents()
        {
            this._domainEvents.Clear();
        }
    }
}
